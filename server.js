const express = require('express');
const axios = require("axios");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());

const articleServiceHost = "http://localhost:3000/article/feed";
const reviewServiceHost = "http://localhost:4001/feed";

app.post("/", async (req, res) => {
    console.log("received request for feed");
    let following = req.body.following;
    let get_all=req.body.get_all;
    if (following) following = following.map((relation) => relation.user_id);
    let articles = [];
    await axios.post(articleServiceHost, {
        following: following,
        get_all,
    }).then((response) => {
        articles = response.data.articles;
    }).catch((error) => console.log(error));

    let reviews = [];
    await axios.post(reviewServiceHost, {
        following: following
    },).then((response) => {
        reviews = response.data.reviews;
    }).catch((error) => console.log(error));

    res.status(200).send({
        articles: articles,
        reviews: []
    })
});

const port = process.env.PORT || 4005;

app.listen(port, () => {
    console.log("server listening on port " + port);
})